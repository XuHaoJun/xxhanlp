# xxhanlp

## Quciskstart

```shell
poetry env use 3.8.12
poetry install
uvicorn main:app --reload
```

## Heroku depoly

heroku init

```shell
heroku stack:set container --app yourAppName
```

build & push & release nestjs api server

```shell
docker build -t registry.heroku.com/yourAppName/web -f ./Dockerfile .
docker push registry.heroku.com/yourAppName/web
heroku container:release web --app yourAppName
```
