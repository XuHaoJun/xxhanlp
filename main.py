from dotenv import load_dotenv  # nopep8

load_dotenv()  # nopep8

import uvicorn
from pydantic import BaseModel
import os
from fastapi import FastAPI, HTTPException
import sys
import socket
from typing import Optional, List
from pyhanlp import HanLP as hanlpv1
import hanlp


app = FastAPI()

loaded_tasks = None


def load_tasks_on_need():
    global loaded_tasks
    if loaded_tasks == None:
        loaded_tasks = {
            "split_sentences": {
                "model": hanlp.utils.rules.split_sentence,
                "kwargs": {
                    "output_key": 'sentences'
                }
            },
            "tok_fine": {
                "model": hanlp.load(hanlp.pretrained.tok.FINE_ELECTRA_SMALL_ZH),
                "kwargs": {
                    "output_key": 'tok_fine'
                }
            },
            "ner_msra": {
                "model":   hanlp.load(hanlp.pretrained.ner.MSRA_NER_ELECTRA_SMALL_ZH),
                "kwargs": {
                    "input_key": 'tok_fine',
                    "output_key": 'ner_msra'
                }
            }
        }


class AnalysisBody(BaseModel):
    lines: Optional[List[str]] = None
    text: Optional[str] = None
    tasks: Optional[List[str]] = None


@app.post("/analysis")
def analysis(body: AnalysisBody):
    global loaded_tasks
    load_tasks_on_need()
    if (body.text == None and body.lines == None) or (body.text and body.lines):
        raise HTTPException(
            status_code=400, detail="lines or text should one of")
    text = ""
    if body.text:
        text = body.text
    elif body.lines:
        text = "\n".join(body.lines)

    tasks = ["split_sentences", "tok_fine", "ner_msra"]
    if body.tasks != None:
        tasks = body.tasks
        if task[0] != "split_sentences":
            tasks.insert(0, "split_sentences")
    pipeline = hanlp.pipeline()
    for task_name in tasks:
        task = loaded_tasks[task_name]
        model = task["model"]
        kwargs = task["kwargs"]
        pipeline.append(model, **kwargs)
    return pipeline(text)


@app.get("/")
def home():
    return "hello world"


@app.get("/version")
def get_version():
    hostname = socket.gethostname()
    version = f"{sys.version_info.major}.{sys.version_info.minor}"
    return {
        "name": "xxhanlp",
        "host": hostname,
        "version": f"Hello world! From FastAPI running on Uvicorn. Using Python {version}"
    }


class AnalysisExtraKeywordBody(BaseModel):
    text: str


@app.post("/v1/analysis/extract-keyword")
def analysis_old(body: AnalysisExtraKeywordBody):
    kws = hanlpv1.extractKeyword(body.text, 2)
    print(list(kws))
    return list(kws)


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=os.environ.get(
        'PORT', 8000), log_level="info")
