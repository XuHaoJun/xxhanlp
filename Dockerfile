
#
## Base stage
FROM archlinux as base-stage

WORKDIR /app

RUN mkdir /etc/gnupg && \
    echo "honor-http-proxy" > /etc/gnupg/dirmngr.conf && \
    echo "honor-http-proxy" > /etc/pacman.d/gnupg/dirmngr.conf && \
    pacman-key --init && \
    pacman-key --recv-key 0706B90D37D9B881 FBA220DFC880C036 --keyserver keyserver.ubuntu.com && \
    pacman-key --lsign-key 0706B90D37D9B881 FBA220DFC880C036 && \
    pacman --noconfirm -U 'https://geo-mirror.chaotic.cx/chaotic-aur/chaotic-'{keyring,mirrorlist}'.pkg.tar.zst' && \
    echo "[multilib]" >> /etc/pacman.conf && \
    echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf && \
    echo "[chaotic-aur]" >> /etc/pacman.conf && \
    echo "Include = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf && \
    pacman -Syu --noconfirm && \
    pacman -Sy --noconfirm python-poetry pyenv base-devel openssl zlib xz pacman-contrib wget && \
    pacman -Scc --noconfirm && \
    paccache -r

ENV PYTHON_VERSION=3.8.10
ENV PYENV_ROOT="/root/.pyenv"
ENV PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:/app/.venv/bin:${PATH}"
RUN pyenv install $PYTHON_VERSION && pyenv global $PYTHON_VERSION
# for hanlp v1
RUN pacman -Sy --noconfirm jre-openjdk&& \
    pacman -Scc --noconfirm && \
    paccache -r

#
## Models Stage
FROM base-stage as models-stage
RUN mkdir -p .hanlp/ner .hanlp/tok .hanlp/utils .hanlp/transformers
RUN wget "https://www.dropbox.com/s/ol5bk1l7fp6x76n/data-for-1.7.5.zip?dl=1" -O data-for-1.7.5.zip && \
    wget "https://www.dropbox.com/s/3b639i0k2b459fs/hanlp-1.8.3-release.zip?dl=1" -O hanlp-1.8.3-release.zip && \
    wget "https://www.dropbox.com/s/296kzspevup1uqc/msra_ner_electra_small_20220215_205503.zip?dl=1" -O .hanlp/ner/msra_ner_electra_small_20220215_205503.zip && \
    wget "https://www.dropbox.com/s/qbt7ezapu3obwbz/fine_electra_small_20220615_231803.zip?dl=1" -O .hanlp/tok/fine_electra_small_20220615_231803.zip && \
    wget "https://www.dropbox.com/s/g4if5kqckz9awtc/electra_zh_small_20210706_125427.zip?dl=1" -O .hanlp/transformers/electra_zh_small_20210706_125427.zip && \
    wget "https://www.dropbox.com/s/otb4d28rs9nsjec/char_table_20210602_202632.json.zip?dl=1" -O .hanlp/utils/char_table_20210602_202632.json.zip


# wget "https://www.dropbox.com/s/ol5bk1l7fp6x76n/data-for-1.7.5.zip?dl=1" -O .venv/lib/python3.8/site-packages/pyhanlp/static/data-for-1.8.3.zip && \
# wget "https://www.dropbox.com/s/3b639i0k2b459fs/hanlp-1.8.3-release.zip?dl=1" -O .venv/lib/python3.8/site-packages/pyhanlp/static/hanlp-1.8.3-release.zip
# wget "https://www.dropbox.com/s/296kzspevup1uqc/msra_ner_electra_small_20220215_205503.zip?dl=1" -O ./.hanlp/ner/msra_ner_electra_small_20220215_205503.zip && \
# wget "https://www.dropbox.com/s/qbt7ezapu3obwbz/fine_electra_small_20220615_231803.zip?dl=1" -O ./.hanlp/tok/fine_electra_small_20220615_231803.zip


#
## Dependecies Stage
FROM base-stage as dependencies-stage
COPY poetry.lock poetry.toml pyproject.toml ./
RUN poetry env use $PYTHON_VERSION
RUN mkdir -p .venv/lib/python3.8/site-packages/pyhanlp/static/
COPY --from=models-stage /app/hanlp-1.8.3-release.zip .venv/lib/python3.8/site-packages/pyhanlp/static/hanlp-1.8.3-release.zip
COPY --from=models-stage /app/data-for-1.7.5.zip .venv/lib/python3.8/site-packages/pyhanlp/static/data-for-1.8.3.zip
# gcc for hanlp v1, and remove after installed
RUN poetry install --no-dev && pacman -Rcnu --noconfirm gcc


#
## Release Stage
FROM base-stage as release-stage

COPY --from=dependencies-stage /app/.venv .venv
COPY --from=models-stage /app/.hanlp .hanlp

COPY poetry.toml pyproject.toml .python-version ./
COPY *.py ./

ENV HANLP_JVM_XMS="128m"
ENV HANLP_JVM_XMX="128m"

EXPOSE 8000

CMD [ "" ]

ENTRYPOINT [ "poetry", "run", "python", "main.py"]